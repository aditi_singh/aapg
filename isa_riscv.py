import sys
import random
file1 = open("output.s", "w")


#Functional definition of Opcode Checker 
def opcode_checker(opcode):

	oci     	= [line.strip() for line in open("./oci.txt", 'r')]
	ocu 		= [line.strip() for line in open("./ocu.txt", 'r')]
	ocuj		= [line.strip() for line in open("./ocuj.txt", 'r')]
	ocr 		= [line.strip() for line in open("./ocr.txt", 'r')]
	ocs 		= [line.strip() for line in open("./ocs.txt", 'r')]
	ocsb 		= [line.strip() for line in open("./ocsb.txt", 'r')]
	ocsys1		= [line.strip() for line in open("./ocsys1.txt", 'r')]
	ocsys2		= [line.strip() for line in open("./ocsys2.txt", 'r')]
	
	if opcode in oci:
	# ADDI rd,rs1,imm
		j1 = random.randint(0,31)
		if j1 > 0:
				j = j1
		elif j1 == 0:
				j = 31
	    	k1 = random.randint(0,31)
		if k1 > 0:
				k = k1
		elif k1 == 0:
				k = 31
		l1=random.randint(0,4095)
		l=l1
		Instruction = opcode +" x"+ str(j) +" , x"+ str(k) +" , "+ str(l)

	    	file1.write("%s\n" % Instruction)
	        return

	elif opcode in ocu:
	#LUI rd,imm
		j1 = random.randint(0,31)
		if j1 > 0:
			j = j1
		elif j1 == 0:
			j = 31
		l1=random.randint(0,1048575)
		l=l1
		Instruction = opcode + " x" + str(j) + " , " + str(l)
		file1.write("%s\n" % Instruction)
    		return

	elif opcode in ocuj:
	#JAL rd,imm
		j1 = random.randint(0,31)
		if j1 > 0:
			j = j1
		elif j1 == 0:
			j = 31
		l1=random.randint(0,1048575)
		l=l1
		Instruction = opcode + " x" + str(j) + " , " + str(l)
		file1.write("%s\n" % Instruction)
	        return

	elif opcode in ocr:
	#ADD rd, rs1.rs2
		j1 = random.randint(0,31)
		if j1 > 0:
			j = j1
		elif j1 == 0:
			j = 31
    		k1 = random.randint(0,31)
		if k1 > 0:
			k = k1
		elif k1 == 0:
			k = 31
                l1 = random.randint(0,31)
		if l1 > 0:
			l = l1
		elif l1 == 0:
			l = 31
		Instruction = opcode + " x" + str(j) + " , x"+ str(k) +" , x"+ str(l)
		file1.write("%s\n" % Instruction)
    		return

	elif opcode in ocs:
	#SB rs1,rs2,imm
		j1 = random.randint(0,31)
		if j1 > 0:
			j = j1
		elif j1 == 0:
			j = 31
    		k1 = random.randint(0,31)
		if k1 > 0:
			k = k1
		elif k1 == 0:
			k = 31
    		l1 = random.randint(0,31)
    		l=l1
		Instruction = opcode + " x" + str(j) + " , x"+ str(k) +" , "+ str(l)
		file1.write("%s\n" % Instruction)
    		return

	elif opcode in ocsb:
	#BEQ s1,rs2,imm
		j1 = random.randint(0,31)
		if j1 > 0:
			j = j1
		elif j1 == 0:
			j = 31
	        k1 = random.randint(0,31)
		if k1 > 0:
			k = k1
		elif k1 == 0:
			k = 31
   		l1 = random.randint(0,31)
    		l=l1
		Instruction = opcode + " x" + str(j) + " , x"+ str(k) +" , "+ str(l)
		file1.write("%s\n" % Instruction)
		return

	elif opcode in ocsys1:
	#FENCE
		Instruction=opcode
		file1.write("%s\n" % Instruction)
		return

	elif opcode in ocsys2:
	#RECYCLE rd
		k1 = random.randint(0,31)
		if k1 > 0:
			k = k1
		elif k1 == 0:
			k = 31
		Instruction=opcode + " x" + str(k)
		file1.write("%s\n" % Instruction)
		return	
	
	else: 
		Instruction = "UNDEFINED INSTRUCTION"
    		file1.write("%s\n" % Instruction)
		return

#Main Program Starts Here
def random_ASM_generator(choice):

#Inputs for distribution of different instructions classes in the 4KB/8KB instruction page
	list1 =[ ]	
	per1    = 40
	list1.append(per1)
	per2    = 10
	list1.append(per2)
	per3    = 30
	list1.append(per3)
	per4    = 20
	list1.append(per4)
#in the list1= [per1,per2,per3,per4]
	per	= per1 + per2 + per3 + per4 

	if per > 100:
		sys.exit("Bye ! Next Time please provide correct inputs aggregating 100 or less")

	#ps = int(raw_input("Please provide the page size in KB: "))
	ps =4
	pagesize = ps*256

#Calculating Actual Number of Instructions in a page as per input percentage distributions
	num1 = pagesize*per1/100
#print num1
	num2 = pagesize*per2/100
#print num2
	num3 = pagesize*per3/100
#print num3
	num4 = pagesize*per4/100
#print num4

	num	= num1 + num2 + num3 + num4 
#print num

#Calculating number of nop required #incase of truncation
	if num < pagesize:
		num5 = pagesize - num
	else:
		num5 = 0
#print num14

#grouping as per the variety of branch instructions
	octyp1 = [line.strip() for line in open("./octyp1.txt", 'r')]	# integer computational instructions
	size1 = len(octyp1)
	octyp2 = [line.strip() for line in open("./octyp2.txt", 'r')]	#control transfer instructions
	size2 = len(octyp2)
	octyp3 = [line.strip() for line in open("./octyp3.txt", 'r')]	#load and store instructions
	size3 = len(octyp3)
	octyp4 = [line.strip() for line in open("./octyp4.txt", 'r')]	# system instructions
	size4 = len(octyp4)
	
	#choice = int(raw_input("Please provide the seed for PRNG: "))
	random.seed(choice)	

	count = 0
	PC = 0
	while (count <= (pagesize - 1 - num5)): #incase of no truncation
 		i= random.randint(0,3)
		if  list1[i] != 0:	#incase that actual number of insturctions on that page!=0
			if ((i == 0) and (num1 > 0)): #for page 1
				j=random.randint(0,size1-1)
				opcode_checker(octyp1[j])
				PC = PC + 4
				num1=num1-1
				count=count+1
			elif ((i == 1) and (num2 > 0)):
				j=random.randint(0,size2-1)
				opcode_checker(octyp2[j])
				PC = PC + 4
				num2=num2-1
				count=count+1
			elif ((i == 2) and (num3 > 0)):
				j=random.randint(0,size3-1)
				opcode_checker(octyp3[j])
				PC = PC + 4
				num3=num3-1
				count=count+1
			elif ((i == 3) and (num4 > 0)):
				j=random.randint(0,size4-1)
				opcode_checker(octyp4[j])
				PC = PC + 4
				num4=num4-1
				count=count+1
			
	while ( num5 > 0):
		Instruction = "NOP"
		num5 = num5 - 1
		PC = PC + 4
		file1.write("%s\n" % Instruction)

	return

if __name__ == '__main__':
   choice = sys.argv[1]
   random_ASM_generator(choice)
file1.close()

#file2=open("output.s","r")
#words_list =[]
#contents = file2.readlines()
#for i in range(len(contents)):
    # words_list.extend(contents[i].split()) 
 #   current_line=contents[i].split()
  #  for j in range(0,i):
   # 	current_line[j]=45#abi_check(current_line[j])
#file2.close()
Aditi Singh
