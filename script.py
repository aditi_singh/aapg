import sys
import random
file1 = open("output_final.c", "w")
file1.write("#include <stdio.h>\n")
file1.write("#include <stdlib.h>\n")
file1.write("int main() {\n")
file1.write("\n")
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    file1.write("__asm__(\"")
    file1.write(line)
    file1.write("\");\n")
file1.write("}")
file1.close()
